﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.NopStation.Documentation.Models;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.NopStation.Documentation.Controllers
{
    public class DocumentationController : BasePluginController
    {
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public ActionResult Configure()
        {
            var model = new ConfigurationModel();
            return View("~/Plugins/Plugin.Documentation/Views/Configure.cshtml", model);
        }

    }
}
