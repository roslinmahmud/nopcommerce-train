﻿using Nop.Core;
using Nop.Services.Plugins;

namespace Nop.Plugin.NopStation.Documentation
{
    public class LearnDocumentation : BasePlugin
    {
        private readonly IWebHelper _webHelper;
        public LearnDocumentation(IWebHelper webHelper)
        {
            _webHelper = webHelper;
        }
        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/Documentation/Configure";
        }
    }
}
