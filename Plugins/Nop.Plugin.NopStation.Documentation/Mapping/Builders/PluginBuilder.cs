﻿using FluentMigrator.Builders.Create.Table;
using Nop.Data.Mapping.Builders;
using Nop.Plugin.NopStation.Documentation.Domains;

namespace Nop.Plugin.NopStation.Documentation.Builders
{
    public class PluginBuilder : NopEntityBuilder<CustomTable>
    {
        #region Methods
    
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
        }

        #endregion
    }
}