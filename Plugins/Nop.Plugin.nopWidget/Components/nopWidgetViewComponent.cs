﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.nopWidget.Components
{
    [ViewComponent(Name = "nopWidget")]
    public class nopWidgetViewComponent : NopViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/Plugins/nopWidget/Views/WidgetInfo.cshtml");
        }
        
    }
}
